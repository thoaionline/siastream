# source: https://docs.brew.sh/Node-for-Formula-Authors#example

require "language/node"

class Siastream < Formula
  desc "Next-gen cloud storage for your media"
  homepage "https://siastream.tech"
  url "https://registry.npmjs.org/siastream/-/siastream-1.0.1.tgz"
  version "1.0.1"
  sha256 "763bb1b340e8aa1ce37094d0b47ab1f4d62fcef2f60cc645d4ed9dbc15246b23"

  depends_on "node"

  def install
    system "npm", "install", *Language::Node.std_npm_install_args(libexec)
    bin.install_symlink Dir["#{libexec}/bin/*"]
  end

  test do
    raise "Test not implemented."
  end
end
