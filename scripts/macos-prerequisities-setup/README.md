# Install MacOS Prerequisities

- Execute the script `install-macos-prerequisities.sh` on the MacOS box
- After successful script execution reboot the MacOS box using GUI
  or via command line using `sudo reboot`

The script:

- Installs nvm.
- Installs latest LTS node via nvm
- Fixes Homebrew permissions for the user
- Installs yarn via brew
- Installs FUSE for MacOS (osxfuse) via brew/cask
