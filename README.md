[![npm](https://img.shields.io/npm/v/siastream)](https://www.npmjs.com/package/siastream)

# SiaStream

[SiaStream](https://siastream.tech) lets you store your entire media library in the cloud. It stores your files on providers all around the world to give you unparalleled speed, privacy, and security. It's the gateway to the new Internet - one that you control.

[SiaStream](https://siastream.tech) takes your terabytes and puts them onto [Sia](https://sia.tech), a global cloud storage network.

## About Sia

[Sia](https://sia.tech) is a decentralized storage network that allows users to rent out cloud storage at low, competitive prices. As a permissionless network and open marketplace, it offers some key advantages that makes it a compelling offering to home media server operators like fast streaming speeds.

[Sia](https://sia.tech) is one of the few blockchain products that delivers a real product with real value, and our network has been live for years. It can do all this, and you can take advantage of it for less money than using a traditional cloud storage platform.

### Low Costs

Storing a TB of media on [SiaStream](https://siastream.tech) costs \$3.99 per month, and includes both storage and bandwidth. That buys you an awful lot. You can relieve a home server of its media, or use [SiaStream](https://siastream.tech) as a cloud backup. The data you upload is split apart and encrypted before even leaving your machine, so storage hosts can never see your files. And your files can't be pulled from the network just because your cloud provider changes their mind.

_SiaStream uses the Sia marketplace, storage prices can sometimes temporarily exceed \$3.99 per month._

### No Deplatforming

Any experienced Plex user that has tried hosting their data on Amazon Drive Unlimited or Google Drive Unlimited know that both companies have since changed their policies to remove their unlimited offerings. Users have even been banned by centralized cloud-storage companies due to violations in their policy.

Since [Sia](https://sia.tech) is fully decentralized, it means no single authority can deny you access to storing and streaming files from [SiaStream](https://siastream.tech). In fact, even if SiaStream's parent company shuttered its doors today, the network would continue to function independently. It's built with stability from the ground up.

Learn more about [SiaStream](https://siastream.tech) and [Sia](https://sia.tech) at the [Support Center](https://support.siastream.tech).

## Installation

There are currently 3 ways to run [SiaStream](https://siastream.tech):

- Homebrew
  1. first you will need [brew](https://brew.sh/) installed - follow the instructions on the official website
  1. add our tap to your homebrew environment
     > `brew tap NebulousLabs/siastream https://gitlab.com/NebulousLabs/siastream.git`
  1. install SiaStream
     > `brew install siastream`
  1. once installed, you will be able to run it
     > `siastream`
- NPM
  1. first you will need to install [node](https://nodejs.org) which will also
     install `npm` cli for you
  1. then install SiaStream globally
     > `npm i -g siastream`
  1. once installed, you will be able to run it
     > `siastream`
- download precompiled binaries from the [official website](https://siastream.tech)

## Updating to new version

Every so often we will release an update with new functionalities, bug fixes or
other improvements. Depending on the way you have installed your SiaStream there
are different ways of updating to new version:

- if you downloaded precompiled binaries: download new version from [official website](https://siastream.tech) and overwrite old files with the new ones
- if you installed through homebrew: run `brew update` and then `brew upgrade siastream`
- if you installed through npm: run `npm i -g siastream`

_Remember to restart SiaStream process after upgrading to a new version._

## Development

### Prerequisites

- required: [node](https://nodejs.org) (preferably version 12+)
  - macos: `brew install node` with [brew](https://brew.sh/)
  - linux: [node installation docs](https://nodejs.org/en/download/package-manager/)
- required: [yarn](https://yarnpkg.com)
  - macos: `brew install yarn` with [brew](https://brew.sh/)
  - linux: [yarn installation docs](https://classic.yarnpkg.com/en/docs/install/)

#### FUSE

SiaStream uses FUSE to mount uploaded data from [Sia](https://sia.tech) network to your local directory. Depending on your setup you might need to:

- macos only: [FUSE for mac](https://osxfuse.github.io/)
- linux only: if you're running as a non root user, you need to edit `/etc/fuse.conf` and include `user_allow_other` directive [example](https://askubuntu.com/a/309242)

### Starting development build

- make sure that you meet all the [prerequisites](#prerequisites)
- `yarn` - installs dependencies
- `yarn start` - starts app on localhost:3000

### Building distribution package

- make sure that you meet all the [prerequisites](#prerequisites)
- `yarn` - installs dependencies
- depending on your target environment
  - `yarn package linux` - for linux
  - `yarn package macos` - for macos
- package will be available in `dist` directory under a directory specific to the target platform ex. `dist/siastream-macos-x64` and will contain:
  - `siastream` file - application executable
  - `node_modules` directory - has to be shipped alongside the executable (contains native addons and libraries that could not be bundled in the executable)
