## Sep 24, 2020:
### v1.0.2
**Bugs Fixed**
- fixed an issue with the hosts map crashing the server if it failed extracting geodata from any of the hosts

**Other**
- minor dependencies updates

## Aug 25, 2020:
### v1.0.1
**Key Updates**
- version bump to 1.0.1 due to issues publishing to npm over 1.0.0-rc

## Aug 20, 2020:
### v1.0.0
**Key Updates**
- BREAKING CHANGE: executable file has been renamed from `siastream-app` to `siastream` - please make sure you use correct executable if updating from previous versions
- BREAKING CHANGE: removed `HOST`, `PORT`, `LOG_LEVEL` and `LOG_DIR` environment variables, use new cli options instead (`--host`, `--port`, `--log-level`, `--log-dir`)
- BREAKING CHANGE: changed default siastream and siad data files locations:
    - siastream data files: was `~/siastream/data`, now `~/siastream` (use `--siastream-dir` parameter to set your path if migrating from previous version)
    - siastream internal sia node files: was `~/siastream`, now `~/siastream/sia` (use `--siad-dir` parameter to set your path if migrating from previous version)
- BREAKING CHANGE: SiaStream used to store user media in `/siastream/libraries` siapath, whereas now it won't be adding `/libraries` subdirectory so updating users will need take some steps to ensure your data is accessible:
    - (recommended) download the following script to help with migrating your existing media out of `/libraries`, you will need to run it once while your siastream application is running in the background
        - macos: https://siasky.net/AABneNPb3QNyZ450a1anWEFwRnxtMJlbIc1Ip4kdUH3OnA
        - linux: https://siasky.net/AADupkUl2_6Wxb23_cR3pnzPkFNv-iB6Vz4fzey7BnogGQ
    - (not recommended) OR adjust your siapath accordingly either in setup wizard or in configuration tab (temporarily disabled due to known bug)
- BREAKING CHANGE: due to changes in SiaStream configuration provider, updating users will have to go through setup wizard again to ensure data consistency, it should run auatomatically once you start updated version of siastream
- BREAKING CHANGE: updated minimal version of Sia to 1.5.0 - users not using built in siad will need to update their nodes manually
- improved CLI for production builds, run `siastream --help`
- automatically mount FUSE drive when application starts unless user turns it off in the app deliberately
- [Transak](https://transak.com/) payment system integration - fiat to crypto exchange directly from the app
- when running SiaStream with automatic allowance turned on, every time you reach 90% of your estimated storage size, we will bump it up for you by 20% to continue running smoothly
- added automated daily renter backups and an option to create a backup manually or restore from selected backup from dashboard

**Bugs Fixed**
- fixed an issue with siastream trying to mount a fuse directory on a path that is already mounted - happened when siad did not unmount the directory properly, ie. when the process crashed
- SiaStream will now wait for renter to be ready before trying to upload files
- SiaStream will wait on specific siad modules to be operational before using them
- ensure that fuse mountpoint exists before mounting fuse
- fixes issue where if user toggled fuse from the webapp too fast, it tried to mount it twice
- fixed a bug where fee manager would report disision by zero error for node without any uploaded data
- when running internal siad instance, siad output logger will stay alive even after SiaStream process is killed which allows us to capture closing siad output
- fixed an issue where some settings were not persisted on Settings view

**Other**
- included various help messages, tooltips and support articles to improve user experience
- onboarding went through complete revamp (it is now possible to run the setup again by accessing `/setup` pathname when running siastream)
- improved onboarding seed info screen to explicitly require user confirmation
- default to `~/siastream` as siastream data directory on development build
- added eslint checks as static code analysis to ensure code quality
- included support for Node.js 14
- added new Sia daemon status chip in top right corner to inform about consensus syncing, wallet rescanning or service interruptions
- switched from nexe to pkg as a main binary bundler which allowed us to update bundled node version to 12.18.x
- decreased level of logging for very rarely used messages (`silly`), you can set log level with `--log-level` option when starting siastream
- add monthly deposit alert for users that didn't opt out of automatic allowance management
- Completely new Deposit dialog that includes QR code with wallet address
- restoring wallet from seed is no longer blocking the setup process
- new wallet address is now generated every time the deposit dialog is opened
- added siastream log viewer with option to share log file via skynet
- removed Spending Overview widget due to shift in pricing model making it obsolete
- significant application architecture rewrite [#37](https://gitlab.com/NebulousLabs/siastream/-/merge_requests/37)

