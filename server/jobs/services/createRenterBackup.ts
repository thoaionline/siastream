import retry from "async-retry";
import dayjs from "dayjs";
import { orderBy } from "lodash";
import ms from "ms";
import { Logger } from "winston";
import { getSiaClient } from "../../siaClient";

export default async function createRenterBackup(logger: Logger) {
  const SiaClient = getSiaClient();
  const name = `SiaStream ${dayjs().format("YYYY-MM-DD HH:mm:ss")}`;

  // create a renter backup
  try {
    logger.silly(`Creating ${name} backup`);

    await SiaClient.makeRequest("/renter/backups/create", { name }, "POST", ms("30 minutes"));

    logger.silly(`Backup ${name} created successfully, awaiting 100% completion`);
  } catch (error) {
    const message = error.response ? error.response.body.message : error.message;

    logger.error(`Creating ${name} backup failed`, { message });

    return false;
  }

  // ensure that the backup upload progress is 100% completed, it can take some time
  try {
    const timeout = ms("1 hour"); // this is a long process so we assume it can take up to 1 hour
    const interval = ms("5 seconds"); // check interval time
    const retries = timeout / interval; // calculate number of retries based on timout and interval

    await retry(
      async () => {
        const backups = await SiaClient.RenterBackups();
        const backup = backups.backups.find((backup) => backup.name === name);

        if (!backup) {
          throw new Error(`Could not find backup with name ${name} - has it been succesfully created?`);
        }

        if (backup.uploadprogress < 100) {
          throw new Error(`Backup ${backup.name} not ready, upload progress ${backup.uploadprogress}%`);
        }
      },
      {
        factor: 1, // set to 1 to disable exponential backoff
        randomize: false, // do not randomize the interval time
        minTimeout: interval,
        retries,
      }
    );
  } catch (error) {
    logger.error("Renter backup failed", { message: error.message });

    return false;
  }

  logger.debug(`Renter backup ${name} completed successfully`);

  // remove old backups (any SiaStream backup that goes over the arbitrary limit of keeping 10 latest backups)
  const backups = await retry(() => SiaClient.RenterBackups());
  const siaStreamBackups = backups.backups.find((backup) => backup.name.startsWith("SiaStream"));
  const orderedSiaStreamBackups = orderBy(siaStreamBackups, "creationdate", "desc") as any;
  const redundantSiaStreamBackups = orderedSiaStreamBackups.slice(10);

  redundantSiaStreamBackups.forEach(async (backup) => {
    try {
      await SiaClient.makeRequest("/renter/backups/remove", { name: backup.name }, "POST");
    } catch (error) {
      const message = error.response ? error.response.body.message : error.message;

      logger.error(`Could not remove renter backup ${backup.name}`, { message });
    }
  });

  return true;
}
