import retry from "async-retry";
import expandTilde from "expand-tilde";
import fuseMount from "../api/siad/fuseMount";
import db from "../db";
import winston from "../lib/winston";
import { getSiaClient } from "../siaClient";
import { isSetupComplete } from "./conditions";

const logger = winston.child({ label: "fuse mount job" });

export default async function fuse() {
  // check if the user completed setup
  if (!isSetupComplete(logger)) return;

  const SiaClient = getSiaClient();
  const fuse = await retry(() => SiaClient.FuseSettings()); // get current FUSE state
  const isFuseMounted = Boolean(fuse?.mountpoints?.length);
  const streamConfig = db.stream.getState(); // get local stream config

  // check if FUSE is already mounted / unmounted
  if (streamConfig.mountFuse === isFuseMounted) {
    logger.silly(`FUSE already ${isFuseMounted ? "mounted" : "not mounted"}, skipping`);

    return;
  }

  const fuseMountPath = expandTilde(streamConfig.fuseMountPath);

  try {
    if (streamConfig.mountFuse) {
      await fuseMount();
    } else {
      await SiaClient.UnmountFuse(fuseMountPath);
    }
  } catch (error) {
    logger.error(`FUSE ${streamConfig.mountFuse ? "mounting" : "unmounting"} failed`, {
      path: fuseMountPath,
      error: error.response ? error.response.body.message : error.message,
    });

    return;
  }

  logger.debug(`FUSE ${streamConfig.mountFuse ? "mounted" : "unmounted"} successfully`, { path: fuseMountPath });
}
