import { DEFAUlT_SIASTREAM_DATA_DIR, DEFAUlT_SIAD_DATA_DIR, DEFAUlT_SIASTREAM_LOG_DIR } from "../cli";

// Local application data directory.
export const SIASTREAM_DATA_DIR = process.env.SIASTREAM_DATA_DIR || DEFAUlT_SIASTREAM_DATA_DIR;

// Sia directory is where siad and siac with all their data are located.
export const SIAD_DATA_DIR = process.env.SIAD_DATA_DIR || DEFAUlT_SIAD_DATA_DIR;

// Local application logs directory.
export const SIASTREAM_LOG_DIR = process.env.SIASTREAM_LOG_DIR || DEFAUlT_SIASTREAM_LOG_DIR;
