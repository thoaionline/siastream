import { Client } from "sia-typescript";
import db from "./db";

let client: Client;

export function getSiaClient(): Client {
  if (client) return client;

  return setSiaClient();
}

export function setSiaClient(customConfig = {}): Client {
  const config = db.siad.getState();
  const apiHost = process.env["SIAD_HOST"];
  const apiAuthenticationPassword = process.env["SIAD_API_PASSWORD"];
  const clientConfig = apiHost
    ? { ...config, apiHost, apiAuthentication: true, apiAuthenticationPassword, ...customConfig }
    : { ...config, ...customConfig };

  return (client = new Client(clientConfig));
}
