import fs from "fs";
import path from "path";
import AdmZip from "adm-zip";
import retry from "async-retry";
import execa from "execa";
import ora from "ora"; // eslint-disable-line import/default
import semver from "semver";
import request from "superagent";
import tmp from "tmp";
import { SIASTREAM_LOG_DIR, SIAD_DATA_DIR } from "./lib/paths";
import logger from "./lib/winston";
import { setSiaClient } from "./siaClient";

const semverRegex = /(?<=^v?|\sv?)(?:0|[1-9]\d*)\.(?:0|[1-9]\d*)\.(?:0|[1-9]\d*)(?:-(?:0|[1-9]\d*|[\da-z-]*[a-z-][\da-z-]*)(?:\.(?:0|[1-9]\d*|[\da-z-]*[a-z-][\da-z-]*))*)?(?:\+[\da-z-]+(?:\.[\da-z-]+)*)?(?=$|\s)/gi;
const siaVersion = "1.5.0"; // hardcoded min version
const arch = process.arch.includes("arm") ? "arm" : "amd";
const siaReleaseArchive = `Sia-v${siaVersion}-${process.platform}-${arch}64.zip`;
const downloadUrl = `https://sia.tech/releases/${siaReleaseArchive}`;

function download() {
  const spinner = ora({ prefixText: "Installing sia daemon" }).start();

  return new Promise((resolve, reject) => {
    const dir = tmp.dirSync({ unsafeCleanup: true }); // create tmp directory for sia download

    spinner.text = `Downloading ${downloadUrl}`;

    request
      .get(downloadUrl)
      .on("error", reject)
      .pipe(fs.createWriteStream(path.join(dir.name, siaReleaseArchive)))
      .on("finish", () => {
        spinner.text = "Download complete";

        try {
          // create the target directory for sia binaries and data if it doesn't exist
          if (!fs.existsSync(SIAD_DATA_DIR)) {
            spinner.text = `Creating ${SIAD_DATA_DIR} directory`;

            fs.mkdirSync(SIAD_DATA_DIR, { recursive: true }); // create sia path recursively
          }

          spinner.text = "Extracting sia binaries";

          // load the zip file and extract only siad and siac binaries
          const zip = new AdmZip(path.join(dir.name, siaReleaseArchive));

          zip.extractEntryTo(`Sia-v${siaVersion}-${process.platform}-${arch}64/siac`, SIAD_DATA_DIR, false, true);
          zip.extractEntryTo(`Sia-v${siaVersion}-${process.platform}-${arch}64/siad`, SIAD_DATA_DIR, false, true);

          // make sure siad and siac are executable
          fs.chmodSync(path.join(SIAD_DATA_DIR, "siac"), 0o500); // read + execute by owner
          fs.chmodSync(path.join(SIAD_DATA_DIR, "siad"), 0o500); // read + execute by owner

          dir.removeCallback(); // clean up tmp directory

          spinner.succeed("Done!");

          resolve();
        } catch (error) {
          spinner.fail("Failed!");

          dir.removeCallback(); // clean up tmp directory

          reject(error);
        }
      });
  });
}

function exists() {
  try {
    return fs.existsSync(path.join(SIAD_DATA_DIR, "siad"));
  } catch (error) {
    return false;
  }
}

async function outdated() {
  try {
    const { stdout } = await execa(path.join(SIAD_DATA_DIR, "siad"), ["version"]);
    const matchVersion = stdout.match(semverRegex);

    if (!matchVersion) throw new Error(`Could not extract siad version from the version command, output: ${stdout}`);

    const version = semver.coerce(matchVersion[0]);

    if (!version) throw new Error(`Could not coerce semver from ${matchVersion[0]}`);

    return semver.lt(version, siaVersion);
  } catch (error) {
    logger.error(error.message);

    return false;
  }
}

async function run() {
  const spinner = ora({ prefixText: "Starting Sia" }).start();
  const SiaClient = setSiaClient({ dataDirectory: SIAD_DATA_DIR }); // update client instance
  const logPath = path.resolve(SIASTREAM_LOG_DIR, "siad.log");
  const log = fs.openSync(logPath, "a"); // append to siad.log
  const siad = SiaClient.launch(path.join(SIAD_DATA_DIR, "siad"), { stdio: ["ignore", log, log] });

  spinner.text = `Starting`;

  try {
    // wait until the siad is running and ready, retry the check if necessary
    const { version } = await retry(() => SiaClient.DaemonVersion(), {
      onRetry: (error, retryCount: number) => {
        if (retryCount > 3) {
          spinner.text = `Starting takes longer than usual, check ${logPath}`;
        }
      },
    });

    spinner.succeed(`Running v${version} from ${SIAD_DATA_DIR} (pid ${siad.pid})`);
  } catch (error) {
    spinner.fail(`Failed to start! Check logs in ${SIASTREAM_LOG_DIR}`);
  }
}

export default async () => {
  try {
    // check whether siad exists on given path or is outdated, download if needed
    if (!exists() || (await outdated())) {
      await download();
    }

    // run the daemon and wait until it's ready
    await run();
  } catch (error) {
    logger.error(error);
  }
};
