import { debounce } from "lodash";
import ms from "ms";
import schedule from "node-schedule";
import {
  addFilesForUpload,
  deleteUploadedFiles,
  renterBackup,
  unlockWallet,
  updateEstimatedStorage,
  updateRenterSettings,
  feeManager,
  fuse,
  initialMonthlyDepositAlertConfig,
} from "./jobs";
import winston from "./lib/winston";

const logger = winston.child({ label: "Scheduler" });
const jobs: schedule.Job[] = [];

const EVERY_BLOCK = "*/10 * * * *"; // every 10 minutes - avarage block time
const EVERY_DAY_EARLY_MORNING = "0 4 * * *"; // every day at 4 AM
const EVERY_HALF_A_MINUTE = "*/30 * * * * *"; // every 30 seconds
const EVERY_HALF_AN_HOUR = "*/30 * * * *"; // every 30 minutes
const ONCE_A_WEEK = "0 0 * * 0"; // once a week

export default async function scheduler() {
  await clearScheduler(); // make sure to clear all jobs and watchers before starting

  // start registering files for upload job (every 30 minutes)
  jobs.push(schedule.scheduleJob(EVERY_HALF_AN_HOUR, addFilesForUpload));

  // start delete uploaded files job (once a day at 4 AM)
  // this job also creates backups so we shouldn't run it more than once a day
  jobs.push(schedule.scheduleJob(EVERY_DAY_EARLY_MORNING, deleteUploadedFiles));

  // start renter backup job (once a day at 4 AM)
  // this job creates backups so we shouldn't run it more than once a day
  jobs.push(schedule.scheduleJob(EVERY_DAY_EARLY_MORNING, renterBackup));

  // start the fuse healthcheck job (every 30 minutes)
  jobs.push(schedule.scheduleJob(EVERY_HALF_AN_HOUR, fuse));

  // start the set estimate storage job (once a day at 4 AM)
  jobs.push(schedule.scheduleJob(EVERY_DAY_EARLY_MORNING, updateEstimatedStorage));

  // start the set allowance job (every 30 minutes)
  jobs.push(schedule.scheduleJob(EVERY_HALF_AN_HOUR, updateRenterSettings));

  // start the fee manager job (every 10 minutes - avarage block time)
  jobs.push(schedule.scheduleJob(EVERY_BLOCK, feeManager));

  // start initial monthly deposit alert config job (once a week)
  jobs.push(schedule.scheduleJob(ONCE_A_WEEK, initialMonthlyDepositAlertConfig));

  // start wallet unlock job (every 30 seconds)
  jobs.push(schedule.scheduleJob(EVERY_HALF_A_MINUTE, unlockWallet));

  // run all the jobs immediately once and continue as ascheduled
  invokeJobs();
}

async function clearScheduler() {
  // stop all jobs and clear the queue
  jobs.forEach((job) => job.cancel());
  jobs.length = 0;
}

// debounce 1s not to run this in excess
export const invokeJobs = debounce(async () => {
  logger.debug("Jobs manually invoked, running all jobs");

  jobs.forEach((job) => job.invoke());
}, ms("1 second"));
