import path from "path";
import bodyparser from "body-parser";
import express from "express";
import open from "open"; // eslint-disable-line import/default
import core from "./api/core";
import siad from "./api/siad";
import { SiaStreamArgv } from "./cli";
import db from "./db";
import logger from "./lib/winston";
import scheduler from "./scheduler";
import { getSiaClient } from "./siaClient";
import startDaemon from "./startDaemon";

function startServer(argv: SiaStreamArgv) {
  const server = express();

  server.use(bodyparser.urlencoded({ extended: false }));
  server.use(bodyparser.json());

  server.use("/api", core);
  server.use("/siad", siad);

  // serve frontend assets from build directory (when running in production env)
  server.use(express.static(path.join(__dirname, "..", "build")));
  server.get("*", (req, res) => {
    res.sendFile(path.join(__dirname, "..", "build", "index.html"));
  });

  server.listen(argv.port, argv.host, (error) => {
    if (error) throw error;

    logger.info(`Server listening at http://${argv.host}:${argv.port}`);

    if (process.env.NODE_ENV !== "development") {
      open(`http://${argv.host}:${argv.port}`, { wait: true }).catch((error) => {
        logger.info("Could not open browser window, please go to above address manually");
        logger.debug(error);
      });
    }
  });
}

module.exports = async (argv: SiaStreamArgv) => {
  logger.debug(`Siastream initializing with NODE_ENV=${process.env.NODE_ENV}`);

  const SiaClient = getSiaClient();

  // first we want to check if siad is active
  const running = await SiaClient.isRunning();

  // start siad if it is not already running
  if (!running) await startDaemon();

  // initialize db
  await db.initializeDefaults();

  // run scheduler with all the background tasks
  scheduler();

  // start api server once all the prerequisites are running
  startServer(argv);
};
