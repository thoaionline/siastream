import expandTilde from "expand-tilde";
import { Router } from "express";
import ms from "ms";
import db from "../db";
import { getSiaClient } from "../siaClient";
import { asyncMiddleware } from "./middleware";
import fuseMount from "./siad/fuseMount";

const router = Router();

router.get(
  "/healthcheck",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const running = await SiaClient.isRunning();
    res.send({ running });
  })
);

router.get(
  "/fuse",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const data = await SiaClient.FuseSettings();
    res.send(data);
  })
);

router.post(
  "/fuse/mount",
  asyncMiddleware(async (req, res) => {
    const data = await fuseMount();
    res.status(200).send(data);
  })
);

router.post(
  "/fuse/unmount",
  asyncMiddleware(async (req, res) => {
    const config = db.stream.getState();
    const pathToUnmount = expandTilde(config.fuseMountPath);
    const SiaClient = getSiaClient();
    const data = await SiaClient.UnmountFuse(pathToUnmount);
    res.status(200).send(data);
  })
);

router.get(
  "/consensus",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const data = await SiaClient.Consensus();
    res.send(data);
  })
);

router.get(
  "/gateway",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const data = await SiaClient.Gateway();
    res.send(data);
  })
);

router.get(
  "/version",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const data = await SiaClient.DaemonVersion();
    res.send(data);
  })
);

router.get(
  "/wallet",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const data = await SiaClient.Wallet();
    res.send(data);
  })
);

router.post(
  "/wallet/init",
  asyncMiddleware(async (req, res) => {
    const { encryptionpassword, force } = req.body;
    const SiaClient = getSiaClient();
    const data = await SiaClient.InitWallet(encryptionpassword, "english", force);
    res.send(data);
  })
);

router.post(
  "/wallet/unlock",
  asyncMiddleware(async (req, res) => {
    const { encryptionpassword } = req.body;
    const SiaClient = getSiaClient();
    const data = await SiaClient.UnlockWallet(encryptionpassword);
    res.send(data);
  })
);

router.get(
  "/wallet/verifypassword",
  asyncMiddleware(async (req, res) => {
    const password = req.query.password;
    const SiaClient = getSiaClient();
    const data = await SiaClient.VerifyWalletPassword(password);
    res.send(data);
  })
);

router.post(
  "/wallet/changepassword",
  asyncMiddleware(async (req, res) => {
    const { encryptionpassword, newpassword } = req.body;
    const SiaClient = getSiaClient();
    const data = await SiaClient.ChangePassword(encryptionpassword, newpassword);
    res.send(data);
  })
);

router.get(
  "/wallet/addresses",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const data = await SiaClient.GetOrderedAddresses(5);
    res.send(data);
  })
);

router.get(
  "/wallet/address",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const data = await SiaClient.GetAddress();
    res.send(data);
  })
);

router.get(
  "/renter",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const data = await SiaClient.Renter();
    res.send(data);
  })
);

router.get(
  "/renter/contracts",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const data = await SiaClient.GetContracts();
    res.send(data);
  })
);

router.get(
  "/wallet/seeds",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const data = await SiaClient.makeRequest("/wallet/seeds");
    res.status(200).send(data);
  })
);

router.post(
  "/wallet/init/seed",
  asyncMiddleware(async (req, res) => {
    const { seed, encryptionpassword, force } = req.body;
    const SiaClient = getSiaClient();
    const data = await SiaClient.RestoreWallet(seed, { encryptionpassword, force });
    res.status(200).send(data);
  })
);

router.get(
  "/renter/recoveryscan",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const data = await SiaClient.RecoveryScanProgress();
    res.status(200).send(data);
  })
);

router.get(
  "/daemon/alerts",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const data = await SiaClient.makeRequest("/daemon/alerts");
    res.status(200).send(data);
  })
);

router.get(
  "/daemon/version",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const data = await SiaClient.makeRequest("/daemon/version");
    res.status(200).send(data);
  })
);

router.get(
  "/daemon/update",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const data = await SiaClient.makeRequest("/daemon/update");
    res.status(200).send(data);
  })
);

router.post(
  "/daemon/update",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const timeout = ms("5 minutes"); // extended timeout since it can take a while to update
    const data = await SiaClient.makeRequest("/daemon/update", undefined, "POST", timeout);
    res.status(200).send(data);
  })
);

router.get(
  "/daemon/constants",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const data = await SiaClient.Constants();
    res.status(200).send(data);
  })
);

router.get(
  "/renter/files",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const data = await SiaClient.GetFiles();
    res.status(200).send(data);
  })
);

router.get(
  "/renter/uploads/pause",
  asyncMiddleware(async (_, res) => {
    const SiaClient = getSiaClient();
    await SiaClient.PauseUploads(2 * 60 * 60); // pause for 2 hours
    res.sendStatus(204);
  })
);

router.get(
  "/renter/uploads/resume",
  asyncMiddleware(async (_, res) => {
    const SiaClient = getSiaClient();
    await SiaClient.ResumeUploads();
    res.sendStatus(204);
  })
);

router.get(
  "/renter/backups",
  asyncMiddleware(async (_, res) => {
    const SiaClient = getSiaClient();
    const data = await SiaClient.RenterBackups();
    res.status(200).send(data);
  })
);

router.post(
  "/renter/backups/create",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const { name } = req.body;
    const timeout = ms("30 minutes"); // extended timeout since it can take a while to execute

    await SiaClient.makeRequest("/renter/backups/create", { name }, "POST", timeout);

    res.status(204);
  })
);

router.post(
  "/renter/backups/restore",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const { name } = req.body;
    const timeout = ms("30 minutes"); // extended timeout since it can take a while to execute

    await SiaClient.makeRequest("/renter/backups/restore", { name }, "POST", timeout);

    res.status(204);
  })
);

export default router;
