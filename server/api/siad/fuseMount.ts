import fs from "fs";
import os from "os";
import expandTilde from "expand-tilde";
import db from "../../db";
import logger from "../../lib/winston";
import * as fuse from "../../services/fuse";
import { getSiaClient } from "../../siaClient";

const platform = os.platform();

/**
 * This function will try to unmount fuse directory using native fuse calls.
 */
async function unmount() {
  const config = db.stream.getState();

  try {
    await fuse.unmount(config.fuseMountPath);
  } catch (error) {
    logger.error(`FUSE failed to unmount ${config.fuseMountPath}: ${error.message}`);
  }

  logger.debug(`FUSE succesfully unmounted ${config.fuseMountPath}`);
}

/**
 *
 */
async function mount() {
  // detect whether server is running linux and the process is not run as root
  const allowOther = platform === "linux" && !(process.getuid && process.getuid() === 0);
  const SiaClient = getSiaClient();
  const config = db.stream.getState();
  const fuseMountPath = expandTilde(config.fuseMountPath);

  return SiaClient.MountFuse(fuseMountPath, config.siaStreamSiaPath, true, allowOther);
}

/**
 * This function will try to create a siastream directory in sia. It does not fail
 * if the directory already exists so we don't have to check for existence.
 */
async function ensureSiaPathExists() {
  const SiaClient = getSiaClient();
  const config = db.stream.getState();

  try {
    await SiaClient.CreateDir(config.siaStreamSiaPath);
  } catch (error) {
    const message = `Failed to create siapath ${config.fuseMountPath}`;

    logger.error(`${message}: ${error.message}`);

    throw new Error(message);
  }
}

/**
 * This function will try to create the directory recursively. Creating directory recursively
 * does not fail if the directory already exists so we do not need to check for existence.
 */
async function ensureMountpointExists() {
  const config = db.stream.getState();

  try {
    await fs.promises.mkdir(config.fuseMountPath, { recursive: true });
  } catch (error) {
    const message = `Failed to create fuse mount directory ${config.fuseMountPath}`;

    logger.error(`${message}: ${error.message}`);

    throw new Error(message);
  }
}

export default async function fuseMount() {
  // Make sure siapath exists and create it if it doesn't because fuse will fail to mount otherwise.
  await ensureSiaPathExists();

  // Make sure that the local mountpoint directory exists and create it if it doesn't because fuse will
  // fail to mount otherwise.
  await ensureMountpointExists();

  try {
    return await mount();
  } catch {
    logger.debug(`FUSE failed to mount, trying to unmount manually and retry`);
  }

  /**
   * In case mounting failed we want to try to unmount the directory manually and try again.
   * This is just a wild try because we are not checking whether fuse failed due to the
   * mountpoint being already mounted or due to any other issue however there is no easy way
   * to do it right now and that's the best guess we can do.
   *
   * TODO: detect fuse errors more precisely and try to unmount manually only if necessary
   */

  try {
    await unmount();
    return await mount();
  } catch (error) {
    logger.error(`FUSE failed to mount: ${error.message}`);

    throw error;
  }
}
