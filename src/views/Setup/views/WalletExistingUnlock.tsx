import { Card, CardHeader, CardContent, FormControlLabel, Checkbox, TextField, Box } from "@material-ui/core";
import axios from "axios";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useSiaStreamSettings, useWallet } from "../../../api";
import { useDBConfig } from "../../../states/DBConfig";
import BackButton from "../components/BackButton";
import Navigation from "../components/Navigation";
import NextButton from "../components/NextButton";
import StepTransitionAnimation from "../components/StepTransitionAnimation";

export default function WalletExistingUnlock() {
  const { mutate: mutateStreamConfig } = useSiaStreamSettings("stream");
  const { data: wallet, mutate: mutateWallet } = useWallet({ suspense: true });
  const [autounlock, setAutounlock] = useState(!wallet.unlocked);
  const [password, setPassword] = useState("");
  const [passwordErrorMessage, setPasswordErrorMessage] = useState("");
  const { setStreamConfig } = useDBConfig();
  const history = useHistory();
  const handleNext = async () => {
    setPasswordErrorMessage(""); // reset password error message on submit

    if (wallet.unlocked && !autounlock) {
      history.push("/setup/media");
    }

    if (!password) {
      setPasswordErrorMessage("Password cannot be empty");
      return;
    }

    try {
      const { data } = await axios.get("/siad/wallet/verifypassword", { params: { password } });

      if (!data.valid) {
        setPasswordErrorMessage("Password is invalid");
        return;
      }

      if (wallet.unlocked && data.valid) {
        await mutateStreamConfig(setStreamConfig({ password: autounlock ? password : null }));

        history.push("/setup/media");
        return;
      }
    } catch (error) {
      setPasswordErrorMessage(error.response ? error.response.data.message : error.message);
      return;
    }

    try {
      await axios.post("/siad/wallet/unlock", { encryptionpassword: password });
    } catch (error) {
      setPasswordErrorMessage(error.response ? error.response.data.message : error.message);
      return;
    }

    await mutateStreamConfig(setStreamConfig({ password: autounlock ? password : null }));

    mutateWallet(); // refresh wallet

    history.push("/setup/media");
  };
  const handleAutounlockChange = () => setAutounlock((autounlock) => !autounlock);
  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
    setPasswordErrorMessage(""); // reset password error on password change
  };

  return (
    <StepTransitionAnimation>
      <Card>
        <CardHeader
          title={wallet.unlocked ? "Your wallet is already unlocked" : "Unlock your wallet"}
          titleTypographyProps={{ variant: "h2" }}
        />
        <CardContent>
          <FormControlLabel
            control={
              <Checkbox checked={autounlock} name="autounlock" onChange={handleAutounlockChange} color="primary" />
            }
            label="SiaStream requires the wallet to be unlocked at all times to operate. Allow SiaStream to manage this for you."
          />

          {(autounlock || !wallet.unlocked) && (
            <Box mt={3}>
              <TextField
                label="Password"
                type="password"
                fullWidth
                autoFocus
                required
                error={Boolean(passwordErrorMessage)}
                helperText={passwordErrorMessage}
                autoComplete="current-password"
                onChange={handlePasswordChange}
              />
            </Box>
          )}
        </CardContent>
      </Card>

      <Navigation>
        <BackButton />

        <NextButton onClick={handleNext} />
      </Navigation>
    </StepTransitionAnimation>
  );
}
