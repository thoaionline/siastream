import {
  Card,
  CardHeader,
  CardContent,
  Typography,
  Box,
  Link,
  Divider,
  FormControlLabel,
  FormHelperText,
  Checkbox,
} from "@material-ui/core";
import { round } from "lodash";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { BYTES_IN_TERABYTE } from "sia-typescript";
import { useSiaStreamSettings } from "../../../api";
import { STORAGE_PRICE_USD_PER_TERABYTE_PER_MONTH } from "../../../constants";
import { useDBConfig } from "../../../states/DBConfig";
import BackButton from "../components/BackButton";
import Navigation from "../components/Navigation";
import NextButton from "../components/NextButton";
import StepTransitionAnimation from "../components/StepTransitionAnimation";

export default function Complete() {
  const history = useHistory();
  const { setStreamConfig } = useDBConfig();
  const { data: streamConfig, mutate: mutateStreamConfig } = useSiaStreamSettings("stream", { suspense: true });
  const { data: allowanceConfig } = useSiaStreamSettings("allowance", { suspense: true });
  const [automaticAllowance, setAutomaticAllowance] = useState(streamConfig.automaticAllowance);
  const handleNext = async () => {
    await mutateStreamConfig(setStreamConfig({ automaticAllowance, isComplete: true }));

    history.push("/");
  };

  const expectedStorageTerabytes = round(allowanceConfig.expectedstorage / BYTES_IN_TERABYTE, 2);
  const monthlySiaStreamPrice = round(expectedStorageTerabytes * STORAGE_PRICE_USD_PER_TERABYTE_PER_MONTH, 2);
  const initialSiaStreamPrice = round(monthlySiaStreamPrice * 3, 2);

  return (
    <StepTransitionAnimation>
      <Card>
        <CardHeader title="Setup complete!" titleTypographyProps={{ variant: "h2" }} />
        <CardContent>
          <Typography paragraph>
            Wohoo, almost ready to start enjoying SiaStream. Please read the below carefully to understand how pricing
            works.
          </Typography>

          <Box mb={3}>
            <Typography>
              Estimated storage: <b>{expectedStorageTerabytes} TB</b>
            </Typography>
            <Typography>
              Estimated SiaStream cost per month: <b>${monthlySiaStreamPrice}</b> *
            </Typography>
          </Box>

          <Typography paragraph>
            SiaStream uses the{" "}
            <Link
              href="https://support.sia.tech/article/dk91b0eibc-welcome-to-sia"
              target="_blank"
              rel="noopener noreferrer"
            >
              Sia data storage marketplace
            </Link>
            . At first time setup, you will be entering into a 3-month storage contract with the hosts (storage
            providers). These contracts will renew every month to extend storage by another month. All deposits are to
            be made in{" "}
            <Link
              href="https://support.sia.tech/article/09tpt2hbu7-what-is-a-siacoin"
              target="_blank"
              rel="noopener noreferrer"
            >
              Siacoin
            </Link>
            .
          </Typography>

          <Box mb={3}>
            <Typography>
              Initial deposit: <b>${initialSiaStreamPrice}</b> **
            </Typography>
            <Typography>
              Monthly deposit starting end of first month: <b>${monthlySiaStreamPrice}</b>
            </Typography>
          </Box>

          <Box mb={3}>
            <FormControlLabel
              control={
                <Checkbox
                  color="primary"
                  checked={automaticAllowance}
                  onChange={() => setAutomaticAllowance(!automaticAllowance)}
                />
              }
              label="Allow SiaStream to manage your allowance settings"
            />
            <FormHelperText>
              NOTE: this means SiaStream will override any allowance changes that are made outside of SiaStream.
            </FormHelperText>
          </Box>

          <Box mb={3}>
            <Divider />
          </Box>

          <Typography paragraph variant="body2" color="textSecondary">
            * Important: The pricing above is calculated based on your estimated storage. Your final spending with be
            based on your actual usage. You only pay for what you use. Please check the dashboard to view your spending
            breakdown.
          </Typography>

          <Typography variant="body2" color="textSecondary">
            ** At any point, you have 2 months of storage paid in advance and will act as a buffer for you to renew
            contracts.
          </Typography>
        </CardContent>
      </Card>

      <Navigation>
        <BackButton />

        <NextButton onClick={handleNext}>Go to dashboard</NextButton>
      </Navigation>
    </StepTransitionAnimation>
  );
}
