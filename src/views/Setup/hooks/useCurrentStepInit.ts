import { useContext, useEffect } from "react";
import { DispatchContext } from "../../../states/Context";

/**
 * React hook that will emit "SETUP/STEP/ENTER" with given payload
 * on step init and emit "SETUP/STEP/EXIT" when it exits. Used to
 * store step specific data in context, data that apply only to
 * current step like the help link that is mounted outside of the
 * step view.
 */
export default function useCurrentStepInit(data: object) {
  const dispatch = useContext(DispatchContext);

  useEffect(() => {
    dispatch({ type: "SETUP/STEP/ENTER", payload: data });

    return () => dispatch({ type: "SETUP/STEP/EXIT" });
  }, [dispatch, data]);
}
