import { Button, ButtonProps, CircularProgress } from "@material-ui/core";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import React, { ReactNode } from "react";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    wrapper: {
      position: "relative",
    },
    progress: {
      color: theme.palette.primary.main,
      position: "absolute",
      top: "50%",
      left: "50%",
      marginTop: -12,
      marginLeft: -12,
    },
  })
);

interface Props extends ButtonProps {
  children?: ReactNode | ReactNode[];
  loading?: boolean;
}

export default function NextButton({ children, loading, ...props }: Props) {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <Button type="submit" variant="contained" color="primary" disabled={loading} {...props}>
        {children || "Next"}
      </Button>

      {loading && <CircularProgress size={24} className={classes.progress} />}
    </div>
  );
}
