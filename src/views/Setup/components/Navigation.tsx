import { Box, BoxProps } from "@material-ui/core";
import React, { ReactNode } from "react";

interface Props extends BoxProps {
  children: ReactNode | ReactNode[];
}

export default function Navigation({ children, ...props }: Props) {
  return (
    <Box mt={4} display="flex" justifyContent="space-between" {...props}>
      {children}
    </Box>
  );
}
