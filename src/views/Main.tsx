import { AppBar, Box, Tabs, Tab, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import { useSiaStreamSettings } from "../api";
import { Dashboard } from "../components/Dashboard";
import { DashboardLayout } from "../components/Layouts";
import { SettingsForm } from "../components/SettingsForm";
import useIncreasedEstimatedStorageMonitor from "../hooks/useIncreasedEstimatedStorageMonitor";
import useMonthlyDepositMonitor from "../hooks/useMonthlyDepositMonitor";

interface TabPanelProps {
  children?: React.ReactNode | React.ReactNode[];
  dir: string;
  index: any;
  value: any;
}

function TabPanel({ children, value, index, ...props }: TabPanelProps) {
  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`application-tabpanel-${index}`}
      aria-labelledby={`application-tab-${index}`}
      {...props}
    >
      {value === index && <Box pt={4}>{children}</Box>}
    </Typography>
  );
}

function a11yProps(index: number) {
  return { id: `application-tab-${index}`, "aria-controls": `application-tabpanel-${index}` };
}

const tabs = {
  dashboard: { index: 0, label: "Dashboard" },
  settings: { index: 1, label: "Settings" },
};

export default function Main() {
  const theme = useTheme();
  const [currentTab, setCurrentTab] = useState(tabs.dashboard.index);
  const { data: streamConfig } = useSiaStreamSettings("stream", { suspense: true });
  useIncreasedEstimatedStorageMonitor();
  useMonthlyDepositMonitor();

  // Ensure user finished setup before loading dashboard. Redirect if necessary.
  if (!streamConfig.isComplete) {
    return <Redirect to={{ pathname: "/setup" }} />;
  }

  return (
    <DashboardLayout>
      <AppBar position="static" color="default">
        <Tabs
          value={currentTab}
          onChange={(event, newTab: number) => setCurrentTab(newTab)}
          indicatorColor="primary"
          aria-label="application navigation tabs"
        >
          <Tab label={tabs.dashboard.label} {...a11yProps(tabs.dashboard.index)} />
          <Tab label={tabs.settings.label} {...a11yProps(tabs.settings.index)} />
        </Tabs>
      </AppBar>

      <TabPanel value={currentTab} index={tabs.dashboard.index} dir={theme.direction}>
        <Dashboard />
      </TabPanel>

      <TabPanel value={currentTab} index={tabs.settings.index} dir={theme.direction}>
        <SettingsForm />
      </TabPanel>
    </DashboardLayout>
  );
}
