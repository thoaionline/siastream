import {
  Checkbox,
  FormControlLabel,
  Dialog,
  Button,
  DialogTitle,
  DialogContentText,
  DialogContent,
  DialogActions,
  TextField,
} from "@material-ui/core";
import { useFormik } from "formik";
import ms from "ms";
import { useSnackbar } from "notistack";
import React, { useState, useEffect } from "react";
import * as Yup from "yup";
import { useSiaStreamSettings, useWallet } from "../api";
import { useDBConfig } from "../states/DBConfig";
import { SiaUnlockWallet } from "../util/siafetch";

function useDialogOpenState() {
  const [open, setOpen] = useState(false);
  const { data: wallet, mutate: mutateWallet } = useWallet({ refreshInterval: ms("5 seconds") });
  const { data: streamConfig } = useSiaStreamSettings("stream");
  const shouldUnlockWallet = Boolean(wallet?.encrypted && !wallet?.unlocked);
  const unlocked = Boolean(wallet?.unlocked);

  // if user saved the password, try to unlock the wallet before showing the dialog
  useEffect(() => {
    if (shouldUnlockWallet && streamConfig.password) {
      SiaUnlockWallet({ encryptionpassword: streamConfig.password }).then(
        () => mutateWallet(), // in case the unlock was successful, refresh wallet
        () => setOpen(true) // in case the unlock failed, show the dialog
      );
    }
  }, [shouldUnlockWallet, streamConfig, mutateWallet]);

  // close the dialog once the wallet is unlocked
  useEffect(() => {
    if (unlocked && open) {
      setOpen(false);
    }
  }, [open, unlocked]);

  return open;
}

export const WalletUnlockModal = () => {
  const { enqueueSnackbar } = useSnackbar();
  const { mutate: mutateStreamConfig } = useSiaStreamSettings("stream");
  const { mutate: mutateWallet } = useWallet({ refreshInterval: ms("5 seconds") });
  const { setStreamConfig } = useDBConfig();
  const open = useDialogOpenState();
  const formik = useFormik({
    initialValues: {
      password: "",
      autounlock: true,
    },
    validationSchema: Yup.object({
      password: Yup.string().required("Password cannot be empty"),
      autounlock: Yup.boolean(),
    }),
    onSubmit: async ({ password, autounlock }, { setSubmitting, setFieldError, resetForm }) => {
      try {
        // unlock wallet with provided encryption password
        await SiaUnlockWallet({ encryptionpassword: password });

        // if autounlock has been selected and password is valid (wallet for successfully
        // unlocked) then save password, otherwise set it to empty string which will make
        // sure to prune it if it was previously saved
        await mutateStreamConfig(setStreamConfig({ password: autounlock ? password : null }));

        // refresh wallet to ensure it's state
        mutateWallet();

        // enqueue a success snackbar
        enqueueSnackbar("Sia unlocked successfully", { variant: "success" });

        // reset form so next time it is opened we don't end up with already filled in password
        resetForm();
      } catch (error) {
        setFieldError("password", error.message);
      }

      setSubmitting(false);
    },
  });

  return (
    <Dialog open={open} aria-labelledby="form-dialog-title">
      <form onSubmit={formik.handleSubmit}>
        <DialogTitle id="form-dialog-title">Sia Locked</DialogTitle>
        <DialogContent>
          <DialogContentText>Please unlock your Sia instance with your password below:</DialogContentText>
          <TextField
            label="Enter password"
            type="password"
            name="password"
            autoComplete="current-password"
            autoFocus
            fullWidth
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.password && Boolean(formik.errors.password)}
            helperText={formik.touched.password && formik.errors.password}
            value={formik.values.password}
          />
          <FormControlLabel
            sx={{ pt: 3 }}
            control={
              <Checkbox
                color="primary"
                name="autounlock"
                checked={formik.values.autounlock}
                onChange={formik.handleChange}
              />
            }
            label="Save my password and keep my wallet unlocked"
          />
        </DialogContent>
        <DialogActions>
          <Button type="submit" variant="contained" color="primary" disabled={formik.isSubmitting} fullWidth>
            Unlock
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};
