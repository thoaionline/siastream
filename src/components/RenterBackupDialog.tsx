import {
  Box,
  Dialog,
  Button,
  DialogTitle,
  CircularProgress,
  DialogContent,
  DialogActions,
  List,
  ListItem,
  ListItemText,
  Typography,
  ListItemIcon,
} from "@material-ui/core";
import { RadioButtonChecked, RadioButtonUnchecked } from "@material-ui/icons";
import axios from "axios";
import dayjs from "dayjs";
import { round, orderBy } from "lodash";
import ms from "ms";
import { useSnackbar } from "notistack";
import React, { useState, useContext, Suspense } from "react";
import { useRenterBackups } from "../api";
import assumeSuccessWhenNoImmediateResponse from "../services/assumeSuccessWhenNoImmediateResponse";
import { DispatchContext, StateContext } from "../states/Context";

const RenterBackupDialogContent = ({ selected, setSelected }) => {
  const { data: renterBackups } = useRenterBackups({ suspense: true, refreshInterval: ms("5 seconds") });
  const backups = orderBy(renterBackups.backups, "creationdate", "desc");
  const handleSelection = (name: string) => {
    setSelected((selected: string) => (selected === name ? "" : name));
  };

  const Backups = () => (
    <List dense>
      {backups.map((backup) => (
        <ListItem
          key={backup.creationdate}
          button
          selected={selected === backup.name}
          disabled={backup.uploadprogress < 100}
          onClick={() => handleSelection(backup.name)}
        >
          <ListItemIcon>
            {backup.uploadprogress === 100 ? (
              selected === backup.name ? (
                <RadioButtonChecked color="primary" />
              ) : (
                <RadioButtonUnchecked />
              )
            ) : (
              <CircularProgress size={16} />
            )}
          </ListItemIcon>
          <ListItemText
            primary={backup.name}
            secondary={`${round(backup.uploadprogress, 2)}% - ${new Date(backup.creationdate * 1000).toLocaleString()}`}
          />
        </ListItem>
      ))}
    </List>
  );

  const NoBackups = () => (
    <Box textAlign="center" color="text.secondary">
      <Typography>no backups found</Typography>
    </Box>
  );

  return backups.length ? <Backups /> : <NoBackups />;
};

export const RenterBackupDialog = () => {
  const { enqueueSnackbar } = useSnackbar();
  const [selected, setSelected] = useState("");
  const [processing, setProcessing] = useState(false);
  const state = useContext(StateContext);
  const dispatch = useContext(DispatchContext);

  const handleClose = () => {
    setSelected("");
    dispatch({ type: "RENTER_BACKUP_DIALOG/CLOSE" });
  };
  const handleCreateBackup = async () => {
    const name = `SiaStream ${dayjs().format("YYYY-MM-DD HH:mm:ss")}`;

    try {
      setProcessing(true);

      // this is a long running call so we should assume success and continue in background if the request didn't fail immediately
      await assumeSuccessWhenNoImmediateResponse(axios.post("siad/renter/backups/create", { name }));

      enqueueSnackbar(`Creating ${name} backup, it can take several minutes...`, { variant: "info" });
      handleClose();
    } catch (error) {
      const message = error.response ? error.response.data.message : error.message;

      enqueueSnackbar(`Creating ${name} backup failed with error: ${message}`, { variant: "error" });
    }

    setProcessing(false);
  };
  const handleRestoreBackup = async () => {
    setProcessing(true);

    try {
      // this is a long running call so we should assume success and continue in background if the request didn't fail immediately
      await assumeSuccessWhenNoImmediateResponse(axios.post("siad/renter/backups/restore", { name: selected }));

      enqueueSnackbar(`Restoring from ${selected}, recovered files will appear over time`, { variant: "info" });
      handleClose();
    } catch (error) {
      const message = error.response ? error.response.data.message : error.message;

      enqueueSnackbar(`Restoring from ${selected} failed with error: ${message}`, { variant: "error" });
    }

    setProcessing(false);
  };

  return (
    <Dialog open={state.renterBackupDialogOpened} maxWidth="md" scroll="paper" onClose={handleClose}>
      <DialogTitle>Renter backups</DialogTitle>
      <DialogContent>
        <Suspense
          fallback={
            <Box display="flex" justifyContent="center">
              <CircularProgress size={16} />
            </Box>
          }
        >
          <RenterBackupDialogContent selected={selected} setSelected={setSelected} />
        </Suspense>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button color="primary" onClick={handleCreateBackup} disabled={processing}>
          Create manual backup
        </Button>
        <Button color="primary" onClick={handleRestoreBackup} disabled={!selected || processing}>
          Restore
        </Button>
      </DialogActions>
    </Dialog>
  );
};
