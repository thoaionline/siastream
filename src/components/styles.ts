/** @jsx jsx */
import css from "@emotion/css";

export const centeredStyle = {
  position: "absolute",
  top: "50%",
  transform: "translateY(-50%)",
};

export const cssDefaultGrid = css`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 32px;
  row-gap: 32px;
`;
