/** @jsx jsx */
import css from "@emotion/css";
import { Tooltip } from "@material-ui/core";
import InfoOutlinedIcon from "@material-ui/icons/InfoOutlined";
import { Box, Flex, jsx } from "theme-ui";
import { CardBody, CardFluid, CardHeader, CardTitle } from "../components/Card";
import { cssDefaultGrid } from "../components/styles";
import { DashHeaderStats } from "./DashHeaderStats";
import { HostMap } from "./Maps";
import { Renter } from "./Renter";

export const Dashboard = () => {
  return (
    <Box>
      <DashHeaderStats />
      <Renter />
      <Box css={cssDefaultGrid}>
        <CardFluid
          css={css`
            grid-column-start: 1;
            grid-column-end: 5;
          `}
        >
          <CardHeader>
            <CardTitle>Host Map</CardTitle>
            <Flex>
              <Tooltip title="This is a visualization of your hosts. SiaStream stores your fragmented, encrypted data all over the world. Nothing is more secure.">
                <InfoOutlinedIcon style={{ opacity: 0.5 }} />
              </Tooltip>
            </Flex>
          </CardHeader>
          <CardBody sx={{ p: 0, height: 400, position: "relative" }}>
            <HostMap />
          </CardBody>
        </CardFluid>
      </Box>
    </Box>
  );
};
