/** @jsx jsx */
import { jsx } from "theme-ui";

export const Caps = (p) => {
  return (
    <span
      sx={{
        fontWeight: "heading",
        letterSpacing: 1.2,
        textTransform: "uppercase",
        fontSize: 1,
      }}
      {...p}
    />
  );
};
