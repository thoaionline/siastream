import { Divider, Drawer, Typography, List, ListItem, ListItemText } from "@material-ui/core";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import ms from "ms";
import React, { useContext } from "react";
import { useConsensus, useDaemonVersion, useGateway } from "../api";
import { DispatchContext, StateContext } from "../states/Context";
import { formatNumber } from "../util/utils";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    list: {
      minWidth: 360,
    },
    listItem: {
      padding: theme.spacing(4),
    },
  })
);

export const ContextMenu = () => {
  const classes = useStyles();
  const state = useContext(StateContext);
  const dispatch = useContext(DispatchContext);
  const { data: consensus } = useConsensus({ refreshInterval: ms("20 seconds") });
  const { data: gateway } = useGateway({ refreshInterval: ms("20 seconds") });
  const { data: version } = useDaemonVersion({ refreshInterval: ms("20 seconds") });

  return (
    <Drawer anchor="right" open={state.sidebarOpened} onClose={() => dispatch({ type: "SIDEBAR/CLOSE" })}>
      <List className={classes.list}>
        <ListItem className={classes.listItem}>
          <ListItemText primary={<Typography variant="h2">Sia Status</Typography>} />
        </ListItem>
        <Divider component="li" />
        <ListItem className={classes.listItem}>
          <ListItemText primary="Online" secondary={gateway?.netaddress} />
        </ListItem>
        <Divider component="li" />
        <ListItem className={classes.listItem}>
          <ListItemText primary={`${gateway?.peers?.length} Peers`} secondary="Connected" />
        </ListItem>
        <Divider component="li" />
        <ListItem className={classes.listItem}>
          <ListItemText primary="Synced" secondary={`${consensus && formatNumber(consensus?.height)} Blocks`} />
        </ListItem>
        <Divider component="li" />
        <ListItem className={classes.listItem}>
          <ListItemText primary="Unlocked" secondary="Wallet" />
        </ListItem>
        <Divider component="li" />
        <ListItem className={classes.listItem}>
          <ListItemText primary={`v${version?.version}`} secondary={version?.gitrevision} />
        </ListItem>
        <Divider component="li" />
      </List>
    </Drawer>
  );
};
