/** @jsx jsx */
import css from "@emotion/css";
import { Suspense, Fragment } from "react";
import { Box, jsx } from "theme-ui";
import { cssDefaultGrid } from "../components/styles";
import { DepositAddressModal } from "./DepositAddressModal";
import { FuseCard } from "./FuseCard";
import { RenterBackupDialog } from "./RenterBackupDialog";
import { WalletCard } from "./WalletCard";
import { WalletUnlockModal } from "./WalletUnlockModal";

export const Renter = () => {
  return (
    <Fragment>
      <WalletUnlockModal />
      <DepositAddressModal />
      <RenterBackupDialog />
      <Box
        css={css`
          ${cssDefaultGrid}
          grid-template-rows: 1fr;
        `}
        sx={{ pb: "32px" }}
      >
        <WalletCard
          css={css`
            grid-column-start: 1;
            grid-column-end: 3;
            grid-row-start: 1;
            grid-row-end: 2;
          `}
        />
        <Suspense fallback={null}>
          <FuseCard />
        </Suspense>
      </Box>
    </Fragment>
  );
};
