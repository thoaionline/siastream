/**
 * Utility function that converts number or a numeric string to a formatted number string.
 * We can't use Intl here for now since we need to support big int values.
 */
export function formatNumber(number: number | string): string {
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/**
 * Utility function that checks whether a string looks like a seed string.
 * It is not 100% accurate but for most part it is a good first check.
 */
export function isSeedString(value: string) {
  const words = value.split(" ");
  const count = words.length;

  // seed is a string that consists of 28 or 29 words
  return count === 28 || count === 29;
}
