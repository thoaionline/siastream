import * as yup from "yup";

export const mapYupErrorsToFormik = (e: yup.ValidationError) => {
  return e.inner.reduce((x, y) => {
    return { ...x, [y.path]: y.message };
  }, {});
};
