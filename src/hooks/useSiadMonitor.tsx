import ms from "ms";
import { useSnackbar } from "notistack";
import { useEffect } from "react";
import semver from "semver";
import shortid from "shortid";
import { useDaemonVersion } from "../api";

const SNACKBAR_KEY_OFFLINE = shortid.generate();
const SNACKBAR_KEY_OUTDATED = shortid.generate();
const MIN_SIAD_VERSION = "1.5.0";

export default function useSiadMonitor() {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const { data: daemon, error } = useDaemonVersion({ refreshInterval: ms("1 minute") });

  // display notification about offline siad (could be down or not responding)
  useEffect(() => {
    if (error) {
      enqueueSnackbar("Sia daemon is offline. Please start the daemon. We will continue trying to connect.", {
        variant: "error",
        preventDuplicate: true,
        persist: true,
        key: SNACKBAR_KEY_OFFLINE,
      });
    } else {
      closeSnackbar(SNACKBAR_KEY_OFFLINE);
    }
  }, [enqueueSnackbar, closeSnackbar, error]);

  // display notification about outdated daemon version
  useEffect(() => {
    // use semver.coerce to squash suffixes like rc or master for development siad builds
    if (daemon && semver.lt(semver.coerce(daemon.version), MIN_SIAD_VERSION)) {
      enqueueSnackbar(`Sia is running v${daemon.version} while SiaStream requires at least v${MIN_SIAD_VERSION}.`, {
        variant: "warning",
        preventDuplicate: true,
        persist: true,
        key: SNACKBAR_KEY_OUTDATED,
      });
    } else {
      closeSnackbar(SNACKBAR_KEY_OUTDATED);
    }
  }, [enqueueSnackbar, closeSnackbar, daemon]);
}
