import { Button } from "@material-ui/core";
import filesize from "filesize";
import ms from "ms";
import { useSnackbar } from "notistack";
import React, { useCallback, useEffect } from "react";
import shortid from "shortid";
import { useSiaStreamSettings } from "../api";
import { useDBConfig } from "../states/DBConfig";

const SNACKBAR_KEY = shortid.generate();

export default function useIncreasedEstimatedStorageMonitor() {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const { setStreamConfig } = useDBConfig();
  const { data: allowanceConfig } = useSiaStreamSettings("allowance", {
    suspense: true,
    refreshInterval: ms("30 minutes"),
  });
  const { data: streamSettings, mutate: mutateStreamConfig } = useSiaStreamSettings("stream", {
    suspense: true,
    refreshInterval: ms("30 minutes"),
  });

  const handleSnackbarDismiss = useCallback(async () => {
    // disable increased estimated storage alert
    await mutateStreamConfig(setStreamConfig({ increasedEstimatedStorageAlert: false }));

    closeSnackbar(SNACKBAR_KEY);
  }, [mutateStreamConfig, closeSnackbar, setStreamConfig]);

  useEffect(() => {
    if (streamSettings.increasedEstimatedStorageAlert) {
      const storageSize = filesize(allowanceConfig.expectedstorage, { base: 10 });
      const message = `You have reached 90% of your estimated storage so we increased it to ${storageSize} to continue running SiaStream smoothly`;

      enqueueSnackbar(message, {
        action: <Button onClick={handleSnackbarDismiss}>Dismiss</Button>,
        persist: true,
        variant: "info",
        preventDuplicate: true,
        key: SNACKBAR_KEY,
      });
    } else {
      closeSnackbar(SNACKBAR_KEY);
    }
  }, [enqueueSnackbar, closeSnackbar, mutateStreamConfig, handleSnackbarDismiss, streamSettings, allowanceConfig]);
}
