import { Button } from "@material-ui/core";
import { round } from "lodash";
import ms from "ms";
import { useSnackbar } from "notistack";
import React, { useCallback, useEffect } from "react";
import shortid from "shortid";
import { BYTES_IN_TERABYTE, BLOCKS_PER_MONTH } from "sia-typescript";
import { useConsensus, useSiaStreamSettings, useRenter } from "../api";
import { STORAGE_PRICE_USD_PER_TERABYTE_PER_MONTH } from "../constants";
import { useDBConfig } from "../states/DBConfig";

const SNACKBAR_KEY = shortid.generate();

export default function useMonthlyDepositMonitor() {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const { setStreamConfig } = useDBConfig();
  const { data: consensus } = useConsensus({ refreshInterval: ms("10 minutes") });
  const { data: renter } = useRenter({ refreshInterval: ms("10 minutes") });
  const { data: allowanceConfig } = useSiaStreamSettings("allowance", { suspense: true });
  const { data: streamSettings, mutate: mutateStreamConfig } = useSiaStreamSettings("stream", { suspense: true });

  const consensusHeight = consensus?.height ?? null;
  const renterCurrentPeriod = renter?.currentperiod ?? null;
  const expectedStorage = allowanceConfig.expectedstorage;

  const handleSnackbarDismiss = useCallback(
    async (nextMonthlyDepositHeight: number) => {
      // update next monthly deposit height with a future height
      await mutateStreamConfig(setStreamConfig({ nextMonthlyDepositHeight }));

      closeSnackbar(SNACKBAR_KEY);
    },
    [mutateStreamConfig, closeSnackbar, setStreamConfig]
  );

  useEffect(() => {
    // wait until all apis return valid values
    if (!consensusHeight || !renterCurrentPeriod) return;

    // monthly deposit alerts are only enabled for users who let SiaStream manage their allowance
    if (!streamSettings.automaticAllowance) return;

    // we need to make sure the height of next alert is already set (can be null until it's actually initialized)
    if (!streamSettings.nextMonthlyDepositHeight) return;

    // consensus height not yet reached next monthly deposit alert height
    if (consensusHeight < streamSettings.nextMonthlyDepositHeight) {
      closeSnackbar(SNACKBAR_KEY);

      return;
    }

    // calculate monthly SiaStream price based on estimated storage and create an alert message
    const expectedStorageTerabytes = round(expectedStorage / BYTES_IN_TERABYTE, 2);
    const monthlySiaStreamPrice = expectedStorageTerabytes * STORAGE_PRICE_USD_PER_TERABYTE_PER_MONTH;
    const message = `Monthly deposit alert: Fund your wallet with ${monthlySiaStreamPrice} USD to continue using SiaStream smoothly`;

    // In the case where SiaStream has not been running for at least 2 months, stored nextMonthlyDepositHeight
    // will be 2 months in the past so once user dismissed the monthly deposit message, we should push the
    // nextMonthlyDepositHeight info future because otherwise, he would receive the same message again.
    // Also in case renter.currentperiod doesn't update on time, we want to make sure we use larger from the
    // stored next monthly deposit height and current period height as a base and then we add one month in block
    // height to push the height one month in the future.
    const newNextMonthlyDepositHeight =
      Math.max(streamSettings.nextMonthlyDepositHeight, renterCurrentPeriod) + BLOCKS_PER_MONTH;

    enqueueSnackbar(message, {
      action: <Button onClick={() => handleSnackbarDismiss(newNextMonthlyDepositHeight)}>Dismiss</Button>,
      persist: true,
      variant: "info",
      preventDuplicate: true,
      key: SNACKBAR_KEY,
    });
  }, [
    enqueueSnackbar,
    closeSnackbar,
    mutateStreamConfig,
    handleSnackbarDismiss,
    streamSettings,
    expectedStorage,
    renterCurrentPeriod,
    consensusHeight,
  ]);
}
