import useSWR, { ConfigInterface } from "swr";
import fetcher from "./fetcher";

/**
 * SWR wrapped Sia GET /renter/contracts endpoint react hook for renter contracts data fetching
 * https://sia.tech/docs/#renter-contracts-get
 */
export default function useRenterContracts(config?: ConfigInterface) {
  return useSWR<any>("/siad/renter/contracts", fetcher, config);
}
