import useSWR, { ConfigInterface } from "swr";
import fetcher from "./fetcher";

interface OS {
  platform: "aix" | "darwin" | "freebsd" | "linux" | "openbsd" | "sunos" | "win32";
}

/**
 * SWR wrapped /api/os endpoint react hook for os data fetching
 */
export default function useOS(config?: ConfigInterface) {
  return useSWR<OS>("/api/os", fetcher, config);
}
