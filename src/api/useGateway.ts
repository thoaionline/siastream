import { GatewayGET } from "sia-typescript";
import useSWR, { ConfigInterface } from "swr";
import fetcher from "./fetcher";

/**
 * SWR wrapped Sia GET /gateway endpoint react hook for gateway data fetching
 * https://sia.tech/docs/#gateway-get
 */
export default function useGateway(config?: ConfigInterface) {
  return useSWR<GatewayGET>("/siad/gateway", fetcher, config);
}
